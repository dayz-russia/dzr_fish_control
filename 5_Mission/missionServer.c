modded class MissionServer
{
    ref array<string> m_NullArray = new array<string>;
	string m_NullString = "";
    string m_StringConfig;
    int m_PoleCatchChance;
    int m_PoleFishQuantity;
    //string m_StringFirstLineFromArray;
	override void OnInit()
	{
		//Print("OnInit()");
		super.OnInit();
		
		Print("[dzr_fish_control] ::: Starting Serverside");
		
		string FISHING_SUCCESS = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "FISHING_SUCCESS.txt", "20\r\n\r\nChance to catch something with a fishing pole. 1 to 100.\r\nШанс что-то словить на удочку. От 1 до 100.");
		string FISHING_BAIT_LOSS = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "FISHING_BAIT_LOSS.txt", "2\r\n\r\nChance to lose the bait. 1 to 100.\r\nШанс потерять наживку. 1 to 100.");
		string FISHING_HOOK_LOSS = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "FISHING_HOOK_LOSS.txt", "15\r\n\r\nChance to lose the hook. 1 to 100.\r\nШанс потерять крючок. 1 to 100.");
		string FISHING_DAMAGE = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "FISHING_DAMAGE.txt", "1.5\r\n\r\nRemove this number of health from the rod and the hook after fishing\r\nСколько отнимать здоровья у удочки и крючка.");
		string FISHING_GARBAGE_CHANCE = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "FISHING_GARBAGE_CHANCE.txt", "20\r\n\r\nChance to cath garbage instead of a fish. 1 to 100.\r\nШанс словить мусор вметсо рыбы. 1 to 100.");
		string FISH_QUANTITY_MIN = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "FISH_QUANTITY_MIN.txt", "50\r\n\r\nThe minimum weight percent of the fish caught using a fishing pole. 1 to 100.\r\nМинимальный процент веса пойманной на удочку рыбы. 1 to 100.");
		string FISH_QUANTITY_MAX = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "FISH_QUANTITY_MAX.txt", "100\r\n\r\nThe maximum weight percent of the fish caught using a fishing pole. 1 to 100.\r\Максимальнй процент веса пойманной на удочку рыбы. 1 to 100.");
		string DAYTIME_MODIFIER = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "DAYTIME_MODIFIER.txt", "1\r\n\r\n1 = Better catch in the morning and in the evening.\r\1 = Улов лучше утром и вечером.");
	}
}