modded class ActionConstructor
{
	override void RegisterActions(TTypenameArray actions)
	{
		super.RegisterActions(actions);
		actions.Insert(DZR_ActionFishingNew);
	}
};

modded class FishingRod_Base_New
{
	override void SetActions()
	{
		super.SetActions();
		
		RemoveAction(ActionFishingNew);
		AddAction(DZR_ActionFishingNew);
	}
}

class DZR_ActionDataFishing: ActionData
{
	// ref TStringArray m_NullArray = new TStringArray;
	//string m_NullString = "";
	
	float FISHING_SUCCESS;
	float FISHING_BAIT_LOSS;
	float FISHING_HOOK_LOSS;
	float FISHING_DAMAGE;
	float FISHING_GARBAGE_CHANCE;
	float FISH_QUANTITY_MIN;
	float FISH_QUANTITY_MAX;
	int DAYTIME_MODIFIER;
	
	void InitBait(ItemBase item)
	{
		m_Bait = item;
		m_IsBaitAnEmptyHook = !m_Bait.ConfigIsExisting("hookType");
	}
	
	bool IsBaitEmptyHook()
	{
		return m_IsBaitAnEmptyHook;
	}
	
	void ReadConfig()
	{
		
		FISHING_SUCCESS = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "FISHING_SUCCESS.txt", "").ToFloat() / 100;
		FISHING_BAIT_LOSS = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "FISHING_BAIT_LOSS.txt", "").ToFloat() / 100;
		FISHING_HOOK_LOSS = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "FISHING_HOOK_LOSS.txt", "").ToFloat() / 100;
		FISHING_DAMAGE = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "FISHING_DAMAGE.txt", "").ToFloat();
		FISHING_GARBAGE_CHANCE = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "FISHING_GARBAGE_CHANCE.txt", "").ToFloat() / 100;
		FISH_QUANTITY_MIN = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "FISH_QUANTITY_MIN.txt", "").ToFloat() / 100;
		FISH_QUANTITY_MAX = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "FISH_QUANTITY_MAX.txt", "").ToFloat() / 100;
		DAYTIME_MODIFIER = dzr_fish_control_CFG.GetFile("$profile:\\DZR\\dzr_fish_control", "DAYTIME_MODIFIER.txt", "").ToInt();
		
		Print("===============DZR_ActionDataFishing==============");
		Print(FISHING_SUCCESS);
		Print(FISHING_BAIT_LOSS);
		Print(FISHING_HOOK_LOSS);
		Print(FISHING_DAMAGE);
		Print(FISHING_GARBAGE_CHANCE);
		Print(FISH_QUANTITY_MIN);
		Print(FISH_QUANTITY_MAX);
		Print(DAYTIME_MODIFIER);
		Print("===============DZR_ActionDataFishing==============");
	}
	
	bool 		m_IsSurfaceSea;
	bool 		m_IsBaitAnEmptyHook;
	int 		m_FishingResult = -1;
	float 		m_RodQualityModifier = 0;
	ItemBase 	m_Bait;
	
}

modded class CAContinuousRepeatFishing
{
	void CAContinuousRepeatFishing( float time_to_complete_action )
	{
		m_DefaultTimeToComplete = time_to_complete_action;
	}
	
	override int Execute( ActionData action_data )
	{
		//Print("CAContinuousRepeatFishing | Execute");
		if ( !action_data.m_Player )
		{
			//Print("CAContinuousRepeatFishing | UA_ERROR");
			return UA_ERROR;
		}

		if ( m_TimeElpased < m_TimeToComplete )
		{
			m_TimeElpased += action_data.m_Player.GetDeltaT();
			m_TotalTimeElpased += action_data.m_Player.GetDeltaT();
			//Print("CAContinuousRepeatFishing | Execute | m_TimeElpased: " + m_TimeElpased);
			return UA_PROCESSING;
		}
		else
		{
			m_SpentUnits.param1 = m_TimeElpased;
			SetACData(m_SpentUnits);
			m_TimeElpased = m_TimeToComplete - m_TimeElpased;
			OnCompletePogress(action_data);
			
			DZR_ActionDataFishing fad = DZR_ActionDataFishing.Cast(action_data);
			if (fad && fad.m_FishingResult != -1)
			{
				//Print("CAContinuousRepeatFishing | UA_FINISHED");
				return UA_FINISHED;
			}
			else
			{
				//Print("CAContinuousRepeatFishing | UA_PROCESSING");
				return UA_PROCESSING;
			}
		}
	}
};

class DZR_ActionFishingNewCB : ActionContinuousBaseCB
{
	
	DZR_ActionDataFishing 	m_DZR_ActionDataFishing;
	ref array<string> 	m_JunkTypes = {"Wellies_Black","Wellies_Brown","Wellies_Green","Wellies_Grey","Pot"};
	
	override void CreateActionComponent()
	{
		EnableStateChangeCallback();
		m_ActionData.m_ActionComponent = new CAContinuousRepeatFishing(3.0);
	}
	
	override void EndActionComponent()
	{
		m_DZR_ActionDataFishing = DZR_ActionDataFishing.Cast(m_ActionData);
		m_DZR_ActionDataFishing.ReadConfig();
		
		if (!m_DZR_ActionDataFishing)
		{
			super.EndActionComponent();
			return;
		}
		
		if ( m_DZR_ActionDataFishing.m_State == UA_FINISHED )
		{
			if ( m_DZR_ActionDataFishing.m_FishingResult == 1 )
			SetCommand(DayZPlayerConstants.CMD_ACTIONINT_END);
			else if ( m_DZR_ActionDataFishing.m_FishingResult == 0 )
			SetCommand(DayZPlayerConstants.CMD_ACTIONINT_FINISH);
		}
		else if (m_DZR_ActionDataFishing.m_State == UA_CANCEL )
		{
			ActionContinuousBase action = ActionContinuousBase.Cast(m_DZR_ActionDataFishing.m_Action);
			if(action.HasAlternativeInterrupt())
			{
				SetCommand(DayZPlayerConstants.CMD_ACTIONINT_FINISH);
			}
			else
			{
				SetCommand(DayZPlayerConstants.CMD_ACTIONINT_END);
			}
			m_Canceled = true;
			return;
		}
		else
		{
			if ( m_DZR_ActionDataFishing.m_FishingResult == 1 )
			SetCommand(DayZPlayerConstants.CMD_ACTIONINT_END);
			else if ( m_DZR_ActionDataFishing.m_FishingResult == 0 )
			SetCommand(DayZPlayerConstants.CMD_ACTIONINT_FINISH);
		}
		m_DZR_ActionDataFishing.m_State = UA_FINISHED;
	}
	
	
	void HandleFishingResultSuccess()
	{
		if (!GetGame().IsMultiplayer() || GetGame().IsServer())
		{
			ItemBase fish;
			
			if (!m_DZR_ActionDataFishing.m_Bait)
			m_DZR_ActionDataFishing.InitBait(ItemBase.Cast(m_DZR_ActionDataFishing.m_MainItem.FindAttachmentBySlotName("Hook")));
			
			if (!m_DZR_ActionDataFishing.IsBaitEmptyHook())
			{
				m_DZR_ActionDataFishing.m_Bait.AddHealth(-m_DZR_ActionDataFishing.FISHING_DAMAGE);
				MiscGameplayFunctions.TurnItemIntoItem(m_DZR_ActionDataFishing.m_Bait,m_DZR_ActionDataFishing.m_Bait.ConfigGetString("hookType"),m_DZR_ActionDataFishing.m_Player);
			}
			else
			{
				m_DZR_ActionDataFishing.m_Bait.AddHealth(-m_DZR_ActionDataFishing.FISHING_DAMAGE);
			}
			
			float rnd = Math.RandomFloatInclusive(0.0,1.0);
			if (rnd > m_DZR_ActionDataFishing.FISHING_GARBAGE_CHANCE)
			{
				if (m_DZR_ActionDataFishing.m_IsSurfaceSea)
				fish = ItemBase.Cast(GetGame().CreateObjectEx("Mackerel",m_DZR_ActionDataFishing.m_Player.GetPosition(), ECE_PLACE_ON_SURFACE));
				else
				fish = ItemBase.Cast(GetGame().CreateObjectEx("Carp",m_DZR_ActionDataFishing.m_Player.GetPosition(), ECE_PLACE_ON_SURFACE));
			}
			else
			{
				if ( !m_DZR_ActionDataFishing.m_IsSurfaceSea )
				{
					string junk_type = m_JunkTypes.Get(Math.RandomInt(0,m_JunkTypes.Count()));
					fish = ItemBase.Cast(GetGame().CreateObjectEx(junk_type,m_DZR_ActionDataFishing.m_Player.GetPosition(), ECE_PLACE_ON_SURFACE));
					fish.SetHealth("","Health",fish.GetMaxHealth("","Health") * 0.1);
				}
			}
			
			if (fish)
			{
				//Print("---Caught something: " + fish + "---");
				fish.SetWet(0.3);
				if (fish.HasQuantity())
				{
					float coef = Math.RandomFloatInclusive(m_DZR_ActionDataFishing.FISH_QUANTITY_MIN, m_DZR_ActionDataFishing.FISH_QUANTITY_MAX);
					float item_quantity = fish.GetQuantityMax() * coef;
					item_quantity = Math.Round(item_quantity);
					fish.SetQuantity( item_quantity );
					fish.InsertAgent(eAgents.CHOLERA);
				}
			}
			
			m_DZR_ActionDataFishing.m_MainItem.AddHealth(-m_DZR_ActionDataFishing.FISHING_DAMAGE);
		};
	};
};

class DZR_ActionFishingNew: ActionContinuousBase
{
	private const string ALLOWED_WATER_SURFACES = string.Format("%1|%2", UAWaterType.SEA, UAWaterType.FRESH);
	
	void DZR_ActionFishingNew()
	{
		m_CallbackClass 	= DZR_ActionFishingNewCB;
		m_SpecialtyWeight 	= UASoftSkillsWeight.PRECISE_MEDIUM;
		m_CommandUID 		= DayZPlayerConstants.CMD_ACTIONFB_FISHING;
		m_FullBody 			= true;
		m_StanceMask 		= DayZPlayerConstants.STANCEMASK_ERECT | DayZPlayerConstants.STANCEMASK_CROUCH;
		m_Text 				= "#start_fishing";
	}
	
	override void CreateConditionComponents()  
	{
		m_ConditionItem 	= new CCINonRuined();
		m_ConditionTarget 	= new CCTWaterSurface(UAMaxDistances.LARGE, ALLOWED_WATER_SURFACES);
	}
	
	override bool HasTarget()
	{
		return true;
	}
	
	override bool HasAlternativeInterrupt()
	{
		return true;
	}
	
	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		ItemBase bait;		
		FishingRod_Base_New rod = FishingRod_Base_New.Cast(item);
		
		if (rod)
		bait = ItemBase.Cast(rod.FindAttachmentBySlotName("Hook"));
		
		if (bait && !bait.IsRuined())
		return true;
		
		return false;
	}
	
	override ActionData CreateActionData()
	{
		DZR_ActionDataFishing action_data = new DZR_ActionDataFishing;
		action_data.ReadConfig();
		return action_data;
	}
	
	override bool SetupAction(PlayerBase player, ActionTarget target, ItemBase item, out ActionData action_data, Param extra_data = NULL)
	{
		if ( super.SetupAction( player, target, item, action_data, extra_data))
		{
			vector pos_cursor = action_data.m_Target.GetCursorHitPos();
			if (GetGame().SurfaceIsSea(pos_cursor[0],pos_cursor[2]))
			{
				DZR_ActionDataFishing.Cast(action_data).m_IsSurfaceSea = true;
			}
			FishingRod_Base_New rod = FishingRod_Base_New.Cast(action_data.m_MainItem);
			if (rod)
			{
				DZR_ActionDataFishing.Cast(action_data).m_RodQualityModifier = rod.GetFishingEffectivityBonus();
				DZR_ActionDataFishing.Cast(action_data).InitBait(ItemBase.Cast(action_data.m_MainItem.FindAttachmentBySlotName("Hook")));
			}
			return true;
		}
		return false;
	}
	
	override void OnFinishProgressServer( ActionData action_data )
	{
		action_data.m_Callback.InternalCommand(DayZPlayerConstants.CMD_ACTIONINT_ACTION);
		DZR_ActionDataFishing.Cast(action_data).m_FishingResult = EvaluateFishingResult(action_data);
	}
	
	override void OnFinishProgressClient( ActionData action_data )
	{
		action_data.m_Callback.InternalCommand(DayZPlayerConstants.CMD_ACTIONINT_ACTION);
		DZR_ActionDataFishing.Cast(action_data).m_FishingResult = EvaluateFishingResult(action_data);
	}
	
	override void OnStartClient( ActionData action_data )
	{
		FishingRod_Base_New rod = FishingRod_Base_New.Cast(action_data.m_MainItem);
		rod.AnimateFishingRod(true);
	}
	override void OnStartServer( ActionData action_data )
	{
		FishingRod_Base_New rod = FishingRod_Base_New.Cast(action_data.m_MainItem);
		rod.AnimateFishingRod(true);
	}
	
	override void OnEndClient( ActionData action_data )
	{
		FishingRod_Base_New rod = FishingRod_Base_New.Cast(action_data.m_MainItem);
		rod.AnimateFishingRod(false);
	}
	override void OnEndServer( ActionData action_data )
	{
		FishingRod_Base_New rod = FishingRod_Base_New.Cast(action_data.m_MainItem);
		rod.AnimateFishingRod(false);
	}
	
	override void WriteToContext(ParamsWriteContext ctx, ActionData action_data)
	{
		super.WriteToContext(ctx, action_data);
		
		if( HasTarget() )
		{
			ctx.Write(action_data.m_Target.GetCursorHitPos());
		}
	}
	
	override bool ReadFromContext(ParamsReadContext ctx, out ActionReciveData action_recive_data )
	{		
		super.ReadFromContext(ctx, action_recive_data);
		
		if( HasTarget() )
		{
			vector cursor_position;
			if ( !ctx.Read(cursor_position) )
			return false;
			action_recive_data.m_Target.SetCursorHitPos(cursor_position);
		}
		return true;
	}
	
	int EvaluateFishingResult(ActionData action_data)
	{
		if (action_data.m_Player.IsQuickFishing())
		return 1;
		
		DZR_ActionDataFishing fad = DZR_ActionDataFishing.Cast(action_data);
		fad.ReadConfig();
		
		
		float rnd = fad.m_Player.GetRandomGeneratorSyncManager().GetRandom01(RandomGeneratorSyncUsage.RGSGeneric);
		float m_daytime_modifier = fad.DAYTIME_MODIFIER;
		float hook_modifier = 1;
		float chance;
		
		m_daytime_modifier = GetGame().GetDayTime();
		if ( (m_daytime_modifier > 18 && m_daytime_modifier < 22) || (m_daytime_modifier > 5 && m_daytime_modifier < 9) )
		{
			m_daytime_modifier = 1;
		}
		else
		{
			m_daytime_modifier = 0.5;
		}
		
		//fishing with an empty hook
		if (fad.IsBaitEmptyHook())
		{
			hook_modifier = 0.05;
		}
		
		chance = 1 - (((fad.FISHING_SUCCESS * m_daytime_modifier) + fad.m_RodQualityModifier)) * hook_modifier;
		
		if (rnd > chance)
		{
			return 1;
		}
		else if (rnd < fad.FISHING_BAIT_LOSS && !fad.IsBaitEmptyHook()) // restricts the loss of an empty hook (low chance is enough)
		{
			return 0;
		}
		return -1;
	}
	
};		
